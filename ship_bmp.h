#pragma once
extern BITMAP *ship_bitmaps[NO_SHIP_TYPES][17];
extern RLE_SPRITE *large_ships[16][5];
extern RLE_SPRITE *enemy1_rle[ENEMY1_RLES];
extern RLE_SPRITE *enemy2_rle[ENEMY2_RLES];
extern RLE_SPRITE *enemy3_rle[ENEMY3_RLES];
extern RLE_SPRITE *small1_rle[SMALL1_RLES];
extern BITMAP *superjelly_bmp[2];
