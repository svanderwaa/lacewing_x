#pragma once
void init_actor_as_ship(int i, int ship);

struct ship_list {
  char *ship_name;
  int radius;
  int edge_radius;
  int turning;
  int thrust;
  char slide;
  int mass;
  int recharge;
  int energy;
  int shield_type;
  int shield_strength1;
  int shield_strength2;
  int system1;
  int system2;
  int system3;
  int system1_status1;
  int system2_status1;
  int system3_status1;
  int system1_status2;
  int system2_status2;
  int system3_status2;
  int brake_type;
  int brake_strength;
  int armour1;
};
