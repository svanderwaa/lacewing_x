#include <allegro.h>
//#include "menu_common.h"
#include "glob_struct.h"
#include "palette.h"

//! Copied from menu.c. It's now needed there for menu_trigger.
enum {
  DUEL_OPTIONS,
  DUEL_START,
  DUEL_MODE,
  DUEL_ENEMIES,
  DUEL_HANDICAP_P1,
  DUEL_HANDICAP_P2,
  DUEL_EXIT,
  DUEL_END
};

extern struct gamestruct game;
extern BITMAP *menu_bmp;
extern int menu_index;
extern unsigned char menu_counter;
extern FONT *small_font;
extern FONT *font2;

void menu_display_duel(void) {
  const char *EMPTY = "";
  const char *name = EMPTY;
  const char *value = EMPTY;
  int i = 0;
  int ox = 350, oy = 11, oys = 200;
  int col;

  rectfill(menu_bmp, 180, 50, 440, 400, COLOUR_GREEN1);
  rect(menu_bmp, 180, 50, 440, 400, COLOUR_GREEN6);
  rect(menu_bmp, 179, 49, 441, 401, COLOUR_GREEN7);
  rect(menu_bmp, 178, 48, 442, 402, COLOUR_GREEN8);


  textprintf_centre_ex(menu_bmp, font, 320, 90,
		       COLOUR_ORANGE4 + (menu_counter / 4) % 4, -1, "Duel}");
  textprintf_centre_ex(menu_bmp, font2, 320, 90,
		       COLOUR_RED8 - (menu_counter / 4) % 4, -1, "Duel}");

  for(i = 0; i < DUEL_END; i++) {
    value = EMPTY;
    col = COLOUR_GREEN6;

    if(i == menu_index)
      col = COLOUR_YELLOW8;

    switch (i) {
    case DUEL_OPTIONS:
      col = COLOUR_GREY6;
      name = "Duel Options - Space or Enter to select";
      break;
    case DUEL_MODE:
      name = "Victory goes to the ";
      switch (game.duel_mode) {
      case DUEL_3_MINUTES:
	value = "winner after 3 minutes";
	break;
      case DUEL_10_MINUTES:
	value = "winner after 10 minutes";
	break;
      case DUEL_10_POINTS:
	value = "first to 10 points";
	break;
      case DUEL_30_POINTS:
	value = "first to 30 points";
	break;
      }
      break;
    case DUEL_ENEMIES:
      name = "Enemies - ";
      switch (game.duel_crawlies) {
      case 0:
	value = "None";
	break;
      case 1:
	value = "A few";
	break;
      case 2:
	value = "Plentiful";
	break;
      case 3:
	value = "Swarming";
	break;
      }
      break;
    case DUEL_HANDICAP_P1:
      name = "Player 1 Hull - ";
      switch (game.duel_handicap[0]) {
      case 0:
	value = "75%";
	break;
      case 1:
	value = "100%";
	break;
      case 2:
	value = "120%";
	break;
      case 3:
	value = "150%";
	break;
      }
      break;
    case DUEL_HANDICAP_P2:
      name = "Player 2 Hull - ";
      switch (game.duel_handicap[1]) {
      case 0:
	value = "75%";
	break;
      case 1:
	value = "100%";
	break;
      case 2:
	value = "120%";
	break;
      case 3:
	value = "150%";
	break;
      }
      break;
    case DUEL_START:
      name = "Start Duel";
      break;
    case DUEL_EXIT:
      name = "Exit";
      break;
    default:
      break;
    }

    textprintf_ex(menu_bmp, small_font, ox - 150, oys + oy * i, col, -1,
		  name);
    textprintf_right_ex(menu_bmp, small_font, ox + 45, oys + oy * i, col, -1,
			value);

  }
}
