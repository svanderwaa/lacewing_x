#pragma once
#include "glob_const.h"
//Only because of: 87:NO_CMDS, 126:NO_UPGRADES, 272:NO_ENEMY_ATTRUBUTES

struct armoury {
  char *name;
  int type;
  int speed;
  int timeout;
  int damage;
  int mass;
};

struct optionstruct {
  int sound_init;		// if 0, sound isn't initialised at all. Changed in config file only.
  int sound_mode;		// mono, stereo, reversed, off
  int run_vsync;		// on or off
  int sound_volume;		// sound fx volume; if 0 sound effects not played
  int ambience_volume;		// if 0 ambience not played
};

struct starstruct {
  int colour[2];
  int x[2];
  int y[2];
  char distance[2];
};

struct gamestruct {
  int users;
  int type;
  int duel_mode;
  int duel_crawlies;
  int duel_handicap[2];
  int duel_winner;
  int single_player;
  int ships_left;
  float drag;
};

struct arenastruct {
  int max_x;
  int max_y;
  int level;
  int colour1;
  int colour2;
  int colour3;
  int max_targets_in_level;
  int next_target;
  int total_targets;
  int targets_left_total;
  int target[3];
  int targets_left[3];
  int next_target_upgrade;

  int time_left;
  int level_finished;
  int seconds_left_when_finished;
  int time_bonus;
  int game_over;

  int max_crawlies_in_level;
  int next_crawly;
  int next_crawly_upgrade;
  int crawlies_created;

  int between_target_upgrades;
  int between_crawly_upgrades;

  int hostile;
};


struct playerstruct {
  int actor_controlled;
  int user;
// int ships_left;
  char player_cmd[NO_CMDS];
  char link_fire;
  int link_toggle_delay;

  int ship;
  int score;

  int duel_score;

  int keys[NO_CMDS];
};


struct actorstruct {
  int x, y, x_speed, y_speed, angle, base_angle;
  int moving_angle;		// the angle at which it's moving. Set in drag_actor,
// so may not be very reliable but is good enough for friction burn and
// ai routines etc.

  int type;
  int ship;
  int controller;
  char actor_cmd[NO_CMDS];
  int drag_amount;		// amount that actor is accelerated by drag
  int radius;
  int edge_radius;
  char in_play;

  int turning;
  int thrust;
  int slide;
  int mass;
  int recharge;

  int armour;
  int max_armour;

  int upgrade_slot;
  int upgrades[NO_UPGRADES];	// referred to by slot
  int upgrade_specials[NO_UPGRADES];
  int upgrade_structure[NO_UPGRADES];
  int upgraded_system[MAX_UPGRADES];	// referred to by UPG_ index
  int just_upgraded;
  int just_upgraded_timeout;
  int shield_pulse;
  int recycle1;
  int recycle2;
  int cannon_status;
  int bomb_status;
  int shield;
  int max_shield;
/* int shield_type;
int shield_strength1;
int shield_strength2;
int shield_pulse;
int shield_angle;
int system [3];
int system_status1 [3];
int system_status2 [3];
int max_energy;
int energy;*/
  int brakes;
  int brake_strength;
  char dragging;		// is 1 if drag field active
  int just_collided;
  int spawn_delay;
  int grace_period;
  int repairing;
  int secondary_weapon;

  int seek_x;
  int seek_y;
  int lock;
  int new_lock;

  int sidekick_x[5];
  int sidekick_y[5];
  int sidekick_x_speed[5];
  int sidekick_y_speed[5];
  int sidekick_angle[5];
  int sidekick_recycle;
  int sidekicks;

  int turret_lock;
  int turret_recycle;

  int heavy_recycle;

  int backfire_recycle;

  int exhaust_distance_x;
  int exhaust_distance_y;
// int exhaust_displacement;
  int retro_distance_x;
  int retro_distance_y;
// int retro_displacement;
  int slide_distance_x;
  int slide_distance_y;
// int slide_displacement;
  int flash_distance;

  int engine_demand;

  int orbital_angle;
  int orbital_spin;

  int hurt_pulse;

  int drive_sound[3];
};

struct bulletstruct {
  int x, y, x_speed, y_speed;
  int x2, y2;
  int type;
  int owner;
  int timeout;
  unsigned char seed;
  int damage;
  int left_owner;
  int mass;
  int angle;
  int colours[4];
  int special1;
// missiles: acceleration
// bombs: explosion size
// enemy seeker blobs: actor target
// enemy prong: spin direction (applied in display.c)
  int special2;
// missiles: number of clusters
// bombs: number of clusters
  int special3;
// missiles: seeker target
  int special4;
// missiles: seeker turn rate
  int special5;
// missiles: seeker guidance system
};

struct cloudstruct {
  int x, y, x_speed, y_speed;
  int x2, y2;
  int type;
  unsigned char seed;
  int angle;
  int timeout;
  int tickrate;
  int delta_tickrate;
  int status;
  int colour[5];
};

struct pickupstruct {
  int x, y, x_speed, y_speed;
  int counter;
  int type;
  int timeout;
  int radius;
};

struct enemystruct {

  int x, y, x_speed, y_speed, angle, base_angle;
  int moving_angle;
  int type;
  int subtype;
  int attacking;		// which actor is it attacking (-1 for none)
  int drag_amount;		// amount that enemy is accelerated by drag
  char drag_affected;		// is it affected by drag?
  int radius;
  int edge_radius;
  int recycle;
// char in_play;
  int armour;
  int hurt_pulse;
  int shield;
  int max_shield;
  int attribute[NO_ENEMY_ATTRIBUTES];
  int colours[3];
  int hurt_pulse_colour1;
  int hurt_pulse_colour2;
  unsigned char counter;
  int just_collided;
  int mass;
  int carrying_pickup;
  int burst_fire;
  int target;
};

struct enemy_classification {
  int max_armour;
  int level;
  int common;
  int score;
  int radius;			// for hit calculation
  int edge_radius;		// for edge collision
  int max_shield;
  char drag_affected;
  int mass;
  int default_attributes[NO_ENEMY_ATTRIBUTES];
};
