#include <stdio.h>
#include <allegro.h>
#include "glob_struct.h"
#include "palette.h"
#include "sound.h"

//!! Needs separate header?
extern struct playerstruct player[NO_PLAYERS];
extern struct optionstruct options;
extern BITMAP *menu_bmp;
extern int menu_index;
extern FONT *small_font;

void menu_display_options(void) {
  const char *EMPTY = "";
  const char *name = EMPTY;
  const char *value = EMPTY;
  int i = 0;
  char istr[13];		//This should facilitate a 32-bit integer
  int ox = 350, oy = 9, oys = 75;
  int col;

  rectfill(menu_bmp, 180, 50, 440, 400, COLOUR_GREEN1);
  rect(menu_bmp, 180, 50, 440, 400, COLOUR_GREEN6);
  rect(menu_bmp, 179, 49, 441, 401, COLOUR_GREEN7);
  rect(menu_bmp, 178, 48, 442, 402, COLOUR_GREEN8);

  for(i = 0; i < 32; i++) {
    value = EMPTY;
    col = COLOUR_GREEN6;
    if(i == menu_index)
      col = COLOUR_YELLOW8;

    switch (i) {
    case 0:
      col = COLOUR_GREY6;
      name = "General Options";
      break;
    case 1:
      name = "Sound - ";
      if(options.sound_init == 0)
	value = "Disabled in config file";
      else {
	switch (options.sound_mode) {
	case SOUNDMODE_OFF:
	  value = "Off";
	  break;
	case SOUNDMODE_MONO:
	  value = "On (Mono)";
	  break;
	case SOUNDMODE_STEREO:
	  value = "On (Stereo)";
	  break;
	case SOUNDMODE_REVERSED:
	  value = "On (Reverse Stereo)";
	  break;
	default:
	  value = "Incorrect lacew.cfg(?)";
	  break;
	}
      }
      break;
    case 2:
      name = "Sound Effects Volume - ";
      if(options.sound_volume == 0)
	value = "Off";
      else {
	snprintf(istr, 12, "%i", options.sound_volume);	//!!
	value = istr;
      }
      break;
    case 3:
      name = "Ambience Volume - ";
      if(options.ambience_volume == 0)
	value = "Off";
      else {
	snprintf(istr, 12, "%i", options.ambience_volume);	//!
	value = istr;
      }
      break;
    case 4:
      name = "Video Sync - ";
      if(options.run_vsync == 0)
	value = "Off";
      else
	value = "On";
      break;
    case 5:
      name = "Test Speakers";
      break;
    case 6:
      name = "Test Keys";
      break;
    case 7:
      name = "";		//???
      break;
    case 8:
      col = COLOUR_GREY6;
      name = "Player 1 Keys";
      break;
    case 9:
      name = "Forwards - ";
      value = scancode_to_name(player[0].keys[CMD_THRUST]);
      break;
    case 10:
      name = "Left - ";
      value = scancode_to_name(player[0].keys[CMD_LEFT]);
      break;
    case 11:
      name = "Right - ";
      value = scancode_to_name(player[0].keys[CMD_RIGHT]);
      break;
    case 12:
      name = "Brake - ";
      value = scancode_to_name(player[0].keys[CMD_BRAKE]);
      break;
    case 13:
      name = "Fire Cannon - ";
      value = scancode_to_name(player[0].keys[CMD_FIRE1]);
      break;
    case 14:
      name = "Fire Secondary - ";
      value = scancode_to_name(player[0].keys[CMD_FIRE2]);
      break;
    case 15:
      name = "Upgrade - ";
      value = scancode_to_name(player[0].keys[CMD_UPGRADE]);
      break;
    case 16:
      name = "Slide Left - ";
      value = scancode_to_name(player[0].keys[CMD_LEFT1]);
      break;
    case 17:
      name = "Slide Right - ";
      value = scancode_to_name(player[0].keys[CMD_RIGHT1]);
      break;
    case 18:
      name = "Toggle Linked Fire - ";
      value = scancode_to_name(player[0].keys[CMD_LINK]);
      break;
    case 19:
      col = COLOUR_GREY6;
      name = "Player 2 Keys";
      break;
    case 20:
      name = "Forwards - ";
      value = scancode_to_name(player[1].keys[CMD_THRUST]);
      break;
    case 21:
      name = "Left - ";
      value = scancode_to_name(player[1].keys[CMD_LEFT]);
      break;
    case 22:
      name = "Right - ";
      value = scancode_to_name(player[1].keys[CMD_RIGHT]);
      break;
    case 23:
      name = "Brake - ";
      value = scancode_to_name(player[1].keys[CMD_BRAKE]);
      break;
    case 24:
      name = "Fire Cannon - ";
      value = scancode_to_name(player[1].keys[CMD_FIRE1]);
      break;
    case 25:
      name = "Fire Secondary - ";
      value = scancode_to_name(player[1].keys[CMD_FIRE2]);
      break;
    case 26:
      name = "Upgrade - ";
      value = scancode_to_name(player[1].keys[CMD_UPGRADE]);
      break;
    case 27:
      name = "Slide Left - ";
      value = scancode_to_name(player[1].keys[CMD_LEFT1]);
      break;
    case 28:
      name = "Slide Right - ";
      value = scancode_to_name(player[1].keys[CMD_RIGHT1]);
      break;
    case 29:
      name = "Toggle Linked Fire - ";
      value = scancode_to_name(player[1].keys[CMD_LINK]);
      break;
    case 30:
      name = "";		//??
      break;
    case 31:
      name = "Exit";
      break;
    default:
      break;
    }

    textprintf_ex(menu_bmp, small_font, ox - 150, oys + oy * i, col, -1,
		  name);
    textprintf_right_ex(menu_bmp, small_font, ox + 45, oys + oy * i, col, -1,
			value);
  }
  return;
}
