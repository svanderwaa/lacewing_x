/*
Lacewing
Copyright (C) 2003 Linley Henzell & Captain Pork

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public Licence as published by
the Free Software Foundation; either version 2 of the Licence, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public Licence for more details.

You should have received a copy of the GNU General Public Licence
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The GPL version 2 is included in this distribution in a file called
LICENCE.TXT. Use any text editor or the TYPE command to read it.

You should be able to reach us by sending an email to
captainpork@fastmail.fm.

File: displ_in.c

This file contains:
- bitmap loading and preparation
The bitmaps aren't organised into a datafile because I find datafiles a bit
unwieldy.

*/

#include "allegro.h"

//Strict ANSI math header doesn't define M_PI
#include <math.h>

#include "glob_vars.h"
//Only because of game

#include "display.h"
#include "ship_bmp.h"

#include "palette.h"

extern BITMAP *player1_screen;
extern BITMAP *player2_screen;

extern BITMAP *ship_bitmaps[NO_SHIP_TYPES][17];

/*
White (lines only)
Blue
Green
Amber
Red
*/
extern BITMAP *small2_bmp[SMALL2_BMPS];

extern RLE_SPRITE *rle_title_i;
extern RLE_SPRITE *rle_title_o;
extern RLE_SPRITE *rle_title_s;
extern BITMAP *menu_bmp;
extern FONT *font2;
extern FONT *small_font;

extern BITMAP *upgrade_box1;
extern BITMAP *upgrade_box2;
extern BITMAP *upgrade_box3;

void bitmap_error(const char errtxt[]);

void make_rle_enemy1(BITMAP * source_bmp, int which_enemy, int width,
		     int height);
void make_rle_enemy2(BITMAP * source_bmp, int which_enemy, int width,
		     int height);
void make_rle_enemy3(BITMAP * source_bmp, int which_enemy, int width,
		     int height);
void make_rle_small1(BITMAP * source_bmp, int which_small, int width,
		     int height);
void make_bmp_small2(BITMAP * source_bmp, int which_small, int width,
		     int height);
void make_rle_large_ship(BITMAP * source_bmp, int which_ship);
void make_superjelly_bmps(void);

DATAFILE *datf;

void init_display(void) {
  datf = load_datafile("./gfx/data.dat");
  if(datf == NULL) {
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    allegro_message("Error: Couldn't find data.dat! /n");
    exit(1);
  }

  font = datf[1].dat;
  font2 = datf[0].dat;
  small_font = datf[2].dat;

  textout_centre_ex(screen, font, "Loading}...", 320, 200, COLOUR_GREEN8, -1);
  textout_centre_ex(screen, font2, "Loading}...", 320, 200, COLOUR_YELLOW8,
		    -1);

  BITMAP *temp_bitmap = load_bitmap("gfx//ship8_i.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap");
  }

  BITMAP *temp_bitmap2 = create_bitmap(11, 11);
  if(temp_bitmap2 == NULL) {
    bitmap_error("temp_bitmap2");
  }

  blit(temp_bitmap, temp_bitmap2, 1, 1, 0, 0, 11, 11);
  blit(temp_bitmap, temp_bitmap2, 15, 1, 0, 0, 13, 13);

  int i, j;
  for(i = 0; i < NO_SHIP_TYPES; i++) {
    {
      for(j = 0; j < 17; j++) {
	ship_bitmaps[i][j] = create_bitmap(21, 21);
	if(ship_bitmaps[i][j] == NULL) {
	  bitmap_error("Ship bitmaps 0");
	}
//clear_bitmap(ship_bitmaps [i] [j]);
	blit(temp_bitmap, ship_bitmaps[i][j], (j * 22) + 1, (i * 22) + 1, 0,
	     0, 21, 21);
      }
    }
  }

  destroy_bitmap(temp_bitmap);
  destroy_bitmap(temp_bitmap2);

// Load in enemy bitmaps:

  temp_bitmap = load_bitmap("gfx//enemy1.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (enemy1.bmp not loaded correctly?)");
  }
// temp_bitmap2 = create_bitmap(35, 26);
// if (temp_bitmap2 == NULL)
// {
//  bitmap_error("temp_bitmap2");
// }

  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_STINGER, 35, 26);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_DRIFTER, 27, 27);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_WANDERER, 37, 37);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_SUN, 45, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_SUN_WHITE, 45, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY1, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY2, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY3, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY4, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY5, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_JELLY6, 45, 33);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_MINETHROWER, 35, 41);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_MINETHROWER2, 49, 44);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_CURVER, 39, 36);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_STINGER2, 49, 34);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_BLASTER, 49, 36);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_STINGER3, 29, 28);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_FLAKKER, 45, 37);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_SUPERJELLY, 21, 21);
  make_superjelly_bmps();
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_HIVE, 47, 47);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_STINGER4, 49, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_FLAKKER2, 49, 35);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_BOLTER, 43, 43);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_BOLTER2, 43, 43);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_TORPER1_1, 47, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_TORPER1_2, 47, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_TORPER1_3, 47, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_TORPER1_4, 47, 45);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_HURLER, 43, 25);
  make_rle_enemy1(temp_bitmap, RLE_ENEMY1_HURLER2, 49, 36);

  destroy_bitmap(temp_bitmap);

  temp_bitmap = load_bitmap("gfx//enemy3.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (enemy3.bmp not loaded correctly?)");
  }

  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_FIREBASE, 133, 133);
  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_FIREBASE2, 39, 69);
  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_FIREBASE3, 91, 93);
  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_BOSS1, 175, 159);
  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_BOSS3, 155, 147);
  make_rle_enemy3(temp_bitmap, RLE_ENEMY3_BOSS4, 223, 138);

// blit(temp_bitmap, temp_bitmap2, 1, 1, 0, 0, 35, 26);

// enemy1_rle [RLE_ENEMY1_STINGER] = get_rle_sprite(temp_bitmap2);

// destroy_bitmap(temp_bitmap2);

  destroy_bitmap(temp_bitmap);
// destroy_bitmap(temp_bitmap2);

  temp_bitmap = load_bitmap("gfx//small1.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (small1.bmp not loaded correctly?)");
  }

  for(i = RLE_SMALL1_GREEN_BLOB_L; i < SMALL1_RLES; i++) {
    make_rle_small1(temp_bitmap, i, 11, 11);
  }

  destroy_bitmap(temp_bitmap);


  temp_bitmap = load_bitmap("gfx//small2.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (small2.bmp not loaded correctly?)");
  }

  for(i = BMP_SMALL2_MISSILE_1; i < BMP_SMALL2_SIDE_BOMB + 1; i++) {
    make_bmp_small2(temp_bitmap, i, 7, 7);
  }

  destroy_bitmap(temp_bitmap);

  temp_bitmap = load_bitmap("gfx//enemy2.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (enemy2.bmp not loaded correctly?)");
  }

  for(i = 0; i < 9; i++) {
    make_rle_enemy2(temp_bitmap, i, 27, 27);
  }

  destroy_bitmap(temp_bitmap);

  temp_bitmap = load_bitmap("gfx//lships.bmp", 0);
  if(temp_bitmap == NULL) {
    bitmap_error("temp_bitmap (lships.bmp not loaded correctly?)");
  }

  for(i = 0; i < 16; i++) {
    make_rle_large_ship(temp_bitmap, i);
  }

  destroy_bitmap(temp_bitmap);
  init_messages();

}

void make_rle_enemy1(BITMAP * source_bmp, int which_enemy, int width,
		     int height) {
  BITMAP *temp_bmp = create_bitmap(width, height);
  if(temp_bmp == NULL) {
    bitmap_error("temp_bmp (make_rle_enemy1)");
  }
  blit(source_bmp, temp_bmp, which_enemy * 51 + 1, 1, 0, 0, width, height);
  enemy1_rle[which_enemy] = get_rle_sprite(temp_bmp);
  destroy_bitmap(temp_bmp);
}

void make_rle_enemy2(BITMAP * source_bmp, int which_enemy, int width,
		     int height) {
  BITMAP *temp_bmp = create_bitmap(width, height);
  if(temp_bmp == NULL) {
    bitmap_error("temp_bmp (make_rle_enemy2)");
  }
  clear_bitmap(temp_bmp);
  blit(source_bmp, temp_bmp, which_enemy * 41 + 1, 1, 0, 0, width, height);
  enemy2_rle[which_enemy] = get_rle_sprite(temp_bmp);
  destroy_bitmap(temp_bmp);
}

void make_rle_enemy3(BITMAP * source_bmp, int which_enemy, int width,
		     int height) {
  BITMAP *temp_bmp = create_bitmap(width, height);
  if(temp_bmp == NULL) {
    bitmap_error("temp_bmp (make_rle_enemy3)");
  }

  blit(source_bmp, temp_bmp, which_enemy * 301 + 1, 1, 0, 0, width, height);

  enemy3_rle[which_enemy] = get_rle_sprite(temp_bmp);

  destroy_bitmap(temp_bmp);

}

void make_rle_small1(BITMAP * source_bmp, int which_small, int width,
		     int height) {
  BITMAP *temp_bmp = create_bitmap(width, height);
  if(temp_bmp == NULL) {
    bitmap_error("temp_bmp (make_rle_small1)");
  }

  blit(source_bmp, temp_bmp, which_small * 21 + 1, 1, 0, 0, width, height);

  small1_rle[which_small] = get_rle_sprite(temp_bmp);

  destroy_bitmap(temp_bmp);

}

void make_bmp_small2(BITMAP * source_bmp, int which_small, int width,
		     int height) {

  small2_bmp[which_small] = create_bitmap(width, height);

  if(small2_bmp[which_small] == NULL) {
    bitmap_error("temp_bmp (make_bmp_small2)");
  }

  blit(source_bmp, small2_bmp[which_small], which_small * 21 + 1, 1, 0, 0,
       width, height);

}

void make_rle_large_ship(BITMAP * source_bmp, int which_ship) {
  BITMAP *temp_bmp = create_bitmap(49, 49);
  if(temp_bmp == NULL) {
    bitmap_error("temp_bmp (make_rle_large_ship)");
  }
// if (which_ship > 11)
// {
//  blit(source_bmp, temp_bmp, 51 + 1, 1, 0, 0, 49, 49);
// }
//  else
  blit(source_bmp, temp_bmp, which_ship * 51 + 1, 1, 0, 0, 49, 49);

  int i, x, y, px, base_col = COLOUR_BLUE1;

  for(i = 0; i < 5; i++) {
    switch (i) {
    case 0:
      base_col = COLOUR_BLUE1;
      break;
    case 1:
      base_col = COLOUR_GREEN1;
      break;
    case 2:
      base_col = COLOUR_YELLOW1;
      break;
    case 3:
      base_col = COLOUR_RED1;
      break;
    default:
      break;
    }
    for(x = 0; x < 50; x++) {
      for(y = 0; y < 50; y++) {
	px = getpixel(temp_bmp, x, y);
	if(i == 4) {
	  if(px % 8 != 5 && px != 0)
	    putpixel(temp_bmp, x, y, COLOUR_WHITE);
	  else
	    putpixel(temp_bmp, x, y, 0);
	} else {
	  if(px != 0)
	    putpixel(temp_bmp, x, y, base_col + (px % 8));
	}
      }
    }
//assert( which_ship < 16 && i < 5 )
    large_ships[which_ship][i] = get_rle_sprite(temp_bmp);
  }

  destroy_bitmap(temp_bmp);

}

void make_superjelly_bmps(void) {
  superjelly_bmp[0] = create_bitmap(21, 21);
  if(superjelly_bmp[0] == NULL) {
    bitmap_error("temp_bmp (make_superjelly_bmps - superjelly_bmp [0])");
  }
  clear_bitmap(superjelly_bmp[0]);

  superjelly_bmp[1] = create_bitmap(21, 21);
  if(superjelly_bmp[1] == NULL) {
    bitmap_error("temp_bmp (make_superjelly_bmps - superjelly_bmp [1])");
  }
  clear_bitmap(superjelly_bmp[1]);

  draw_rle_sprite(superjelly_bmp[0], enemy1_rle[RLE_ENEMY1_SUPERJELLY], 0, 0);

  rotate_sprite(superjelly_bmp[1], superjelly_bmp[0], 0, 0, itofix(64));

}

void bitmap_error(const char errtxt[]) {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Bitmap creation error/n%s/n", allegro_error);
  allegro_message(errtxt);
  exit(1);

}

void prepare_display(void) {
  if(game.users == 1) {
    player1_screen = create_bitmap(640, 480);
    clear_bitmap(player1_screen);
    player2_screen = 0;
  } else {
    player1_screen = create_bitmap(315, 480);
    clear_bitmap(player1_screen);
    player2_screen = create_bitmap(315, 480);
    clear_bitmap(player2_screen);
  }
}


void end_game() {
//Contra void prepare_display(void)
  if(game.users == 1) {
    destroy_bitmap(player1_screen);
    player1_screen = 0;
  } else {
    destroy_bitmap(player1_screen);
    destroy_bitmap(player2_screen);
    player1_screen = player2_screen = 0;
  }
}
